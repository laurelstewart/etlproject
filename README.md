# ETLProject

Problem: data about CUIT wireless network utilization comes from multiple sources and is not ready for business analysis.

Data sources: daily emails, API from wireless monitoring tool

Project: combine data to show meaningful statistics regarding utilization over time

